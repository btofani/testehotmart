var root = angular.module('root',['ngRoute']);

root.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	$locationProvider.hashPrefix('');
	$locationProvider.html5Mode(true);
   console.log($routeProvider)
	$routeProvider.when('/', {
		templateUrl : 'public/view/upload.html',
		controller     : 'UploadControl',
	}).when('/lista', {
		templateUrl : 'public/view/lista.html',
		controller  : 'ListaControl',
	}).otherwise ({ 
		redirectTo: '/' 
	});
}]);